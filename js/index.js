class SimonButton { // la class qui permet de crée les buttons
    constructor(id, litColor, color) {

        this.color = color;
        this.id = id;
        this.litColor = litColor;
        this.highlight = function () {
            document.getElementById(id).style.background = litColor;
            setTimeout(function () {
                document.getElementById(id).style.background = color;
            }, vitesseFlash);  // timier qui permet d'ajuster la vitesse du flash
            // 300
        }
    }
}
// ajout des instances de la class SimonButton dans un array;
let buttons = [
    new SimonButton("red", "red", "darkred"),                             // buttons[0];
    new SimonButton("yellow", "yellow", "darkorange"),                   // buttons[1];
    new SimonButton("blue", "lightblue", "darkblue"),                   // buttons[2];
    new SimonButton("green", "lightgreen", "darkgreen")                 // buttons[3];
];


let easyButton = document.querySelector('#easy');
let mediumButton = document.querySelector('#medium');
let hardButton = document.querySelector('#hard');

easyButton.addEventListener('click', function () {
    if (!playing) {
        vitesseFlash = 300;
        vitesseSequence = 650;
    }if (round == 0) {
        vitesseFlash = 300;
        vitesseSequence = 650;
    } else {
        alert("Don't try to cheat");
    }

});

mediumButton.addEventListener('click', function () {
    if (!playing) {
        vitesseFlash = 300;
        vitesseSequence = 550;
    } if (round == 0) {
        vitesseFlash = 300;
        vitesseSequence = 550;
    } else {
        alert("Don't try to cheat");
    }

});

hardButton.addEventListener('click', function () {
    if (!playing) {
        vitesseFlash = 250;
        vitesseSequence = 450;
    } if (round == 0) {
        vitesseFlash = 250;
        vitesseSequence = 450;
    } else {
        alert("Don't try to cheat");
    }

});

let vitesseFlash = 300;
let vitesseSequence = 650; // ajouter de la difficulter au fur a mesure que le jeux augmente

let sequence = [];
let round = 0;
let sequenceIterator = 0;
let userTurn = false;
playing = false;


window.onload = function () {

    // track le boutton newGame
    document.querySelector("#newGame").onclick = function () {
        document.querySelector("#newGame").value = "New Game";
        reset();
        playing = true;
        computerTurn();
    }



    for (let i = 0; i < buttons.length; i++) {

        document.getElementById(buttons[i].id).onclick = function () { //selectionne les id des  intances dans l'array buttons 
            if (playing) {
                if (!userTurn) {
                    alert("Wait the end of the sequence.");
                } else {
                    if (buttons[sequence[sequenceIterator++]].id != this.id) { //
                        console.log(buttons[sequence[sequenceIterator]]);
                        gameOver();
                    }
                    if (sequenceIterator === sequence.length && playing) {
                        sequenceIterator = 0;
                        document.querySelector("#counter").innerHTML = ++round;
                        computerTurn();
                        increaseDifficult();
                    }
                }
            } else {
                alert("Click to the button start for play.");
            }
        }
    }
}

function increaseDifficult() {
    if (round == 4) {
        vitesseSequence -= 50;
    }
    if (round == 7) {
        vitesseSequence -= 50;
    }
    if (round == 10) {
        vitesseSequence -= 50;
    }
}


function computerTurn() { // tour de l'ordi
    userTurn = false;
    extendSequence(1); // function qui ajoute les séquences
    showSequence();     // ajoute une séquence au jeux
}

function reset() {
    sequence = [];
    round = 0;
    sequenceIterator = 0;
    document.querySelector("#counter").innerHTML = round;
}

function gameOver() {
    playing = false;
    alert("Game Over. Try again.");
}

function extendSequence(number) {
    for (i = 0; i < number; i++) {
        sequence.push(Math.floor(Math.random() * 4));      // ajoute un nombre à la séquence
    }
}

function showSequence() {
    let i = 0;
    sequenceLoop();

    function sequenceLoop() {
        setTimeout(function () {
            buttons[sequence[i++]].highlight();
            console.log(buttons[sequence]);
            if (i < sequence.length) {
                sequenceLoop();
            } else {
                userTurn = true;
            }
        }, vitesseSequence); // timer pour ajuster la prochaine sequence 
        //500
    }
}